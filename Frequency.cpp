#include<iostream>
#include<string>
#include<string.h>
#include<stdio.h>
#include<fstream>
#include<vector>
#include<algorithm>
#include"Data_Type.h"
#include"Parsedata.h"
#include"Frequency.h"
#define max 50
using namespace std;

#define i f_i
#define j f_j
#define vec f_vec
#define v f_v
#define Node f_Node

// vector<string>G_vec_item and iterator G_v_item is the global variable used here, below are the local vectors to this file

vector<G_Node>G_Item_Freq;
vector<G_Node>::iterator G_I_Freq;

vector<Node>vec;
vector<Node>::iterator v;

int i=0,j=0;
int pos=0;
Node n;
G_Node n1;

Frequency::Frequency(char *input)
{
   strcpy(input_file,input);
}
 
void Frequency::init_items()
{
   //cout<<G_vec_item[0];

  
   for(i=0;i<G_vec_item.size();i++)
   {
      n.f_item.assign(G_vec_item[i]);
      n.f_val=0;  

      n1.f_item.assign(G_vec_item[i]);
      n1.f_val=0;  

      vec.push_back(n); //local vector
      G_Item_Freq.push_back(n1); //global vector
   
   }   

   
   
}

void Frequency::display()
{
  
  for(i=0;i<vec.size();i++)
  {
       cout<<vec[i].f_item<<vec[i].f_val;
       //cout<<G_Item_Freq[i].f_item<<G_Item_Freq[i].f_val;

  }


}

void Frequency::scan_dataset()
{
   char tran[max];
   FILE *in;
   in=fopen(input_file,"r");
   
   while(fgets(tran,max,in)!=NULL)
   {
     char *ptr=strtok(tran,",");

	     while(ptr!=NULL){
	     //cout<<ptr;
             cal_freq(ptr);
	     ptr=strtok(NULL,",");
	     }
             
   }  //end while

   fclose(in);  

}

void Frequency::cal_freq(char *ch)
{
  //Code snippet for removing the newline

   string temp(ch);
   string str;

   for (i=0; i<temp.length(); i++)
	  {
	    if(temp.at(i)=='\n')
             break;
            
	  }

   str.assign(temp,0,i);

  // Search the item set "vector vec" for the extracted dataset item

  //Check with the global vector, here G_vec_item is the global vector and vec is the local vector to this file

  if((G_v_item=find(G_vec_item.begin(),G_vec_item.end(),str))!=G_vec_item.end()) 
  {
    pos=distance(G_vec_item.begin(),G_v_item);
    vec[pos].f_val++;
    G_Item_Freq[pos].f_val++;
  } 

}

void Frequency::sort_map()
{
   char map_path[max]="map_";
   strcat(map_path,input_file);

   ofstream map;
   map.open(map_path,ofstream::out);
   map<<"Item--Frequency--Map\n";    
   map.close();

   map.open(map_path,ofstream::out|ofstream::app);   

   vector<Node>vec1;
   vector<Node>v1;

   int f_max=0;
   int f_pos=0;
   
   
   for(i=0;i<vec.size();i++)
   {
     vec1.push_back(vec[i]);   

   }

   cout<<endl;

/*   for(i=0;i<vec.size();i++)
   {
     cout<<vec1[i].f_item<<vec1[i].f_val; 

   }*/

   int f_size=vec1.size();

   for(i=0;i<f_size;i++)
   {
	      for(j=0;j<vec1.size();j++){   
  
		  if(vec1[j].f_val>f_max){

	      		f_max=vec1[j].f_val;
                        f_pos=j;
                  }
	      }

    map<<vec1[f_pos].f_item<<"--"<<f_max<<"--"<<i+1<<endl;  
    
    vec1.erase(vec1.begin()+f_pos);
    f_max=0; 
    f_pos=0;
   }

  map.close();
}








