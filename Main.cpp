#include<iostream>
#include<string>
#include<string.h>
#include<stdio.h>
#include<fstream>
#include<vector>
#include<algorithm>
#include<sys/time.h>
#include"Parsedata.h"
#include"Frequency.h"
#define max 50
using namespace std;

int main(int argc, char *argv[])
{
  struct timeval start, end;

  Parsedata p(argv[1]);
  Frequency f(argv[1]);
  
  cout<<"The entered input file is: ";
  cout<<p.input_file<<endl;
  cout<<"The initial data set is:\n";
  p.create_itemset();
  p.display_items();

  f.init_items();
  gettimeofday(&start,NULL);
  f.scan_dataset();
  gettimeofday(&end,NULL);
  cout<<"Time elapsed:"<<((end.tv_sec*1000000+end.tv_usec)-(start.tv_sec*1000000+start.tv_usec))*0.001<<"milli seconds\n";
  f.display();
  f.sort_map();

  return 0;
}
