#ifndef PARSE_DATA
#define PARSE_DATA 

#include<iostream>
#include<string>
#include<string.h>
#include<stdio.h>
#include<fstream>
#include<vector>
#include<algorithm>
#define max 50
using namespace std;

class Parsedata{
 
  public:
  char input_file[max];
  //char items[max];

  Parsedata(char*);
  void create_itemset();
  void compare(char*);
  void display_items();


};

#endif



