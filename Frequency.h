#ifndef FREQUENCY
#define FREQUENCY	

#include<iostream>
#include<string>
#include<string.h>
#include<stdio.h>
#include<fstream>
#include<vector>
#include<algorithm>
#define max 50

class Frequency{

   public:
   char input_file[max];

   Frequency(char*);
   void init_items();
   void display();
   void scan_dataset();
   void cal_freq(char*);
   void sort_map();
};

typedef struct freq_node{

   public:
   string f_item;
   unsigned int f_val;

}f_Node;

#endif
